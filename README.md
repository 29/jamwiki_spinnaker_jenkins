# jamwiki_spinnaker_jenkins

## Description

This deploys Jenkins and Spinnaker into EC2 and creates a Multi-AZ RDS (MySQL) database deployment.

## Usage

These are very rough instructions due to time limitations.

### Jenkins

1. `cd terraform; terraform apply`
This will create a VPC, subnets, RDS database, some IAM roles, an instance for Spinnaker, and an instance for Jenkins.
2. `cd ../jenkins; ansible-playbook -i <JENKINS-IP-ADDR>, --private-key=key.pem jenkins.yaml`
This will install Jenkins, nginx, etc.
3. `ssh -i key.pem ubuntu@<JENKINS-IP-ADDR> sudo cat /var/lib/jenkins/secrets/initialAdminPassword`
4. `ssh -N -L 8080:127.0.0.1:8080 -i key.pem ubuntu@<JENKINS-IP-ADDR>`
5. Open http://localhost:8000
6. Install suggested plugins
7. Configure security as described here: http://www.spinnaker.io/docs/hello-spinnaker
8. Create a new Pipeline using the jamwiki_deb repository (and Jenkinsfile).

### Spinnaker

1. `ssh -N -L 9000:127.0.0.1:9000 -L 8084:127.0.0.1:8084 -L 8087:127.0.0.1:8087 -i key.pem ubuntu@<SPINNAKER-IP-ADDR>`
2. `ssh ubuntu@<SPINNAKER-IP-ADDR>`
3. `sudo stop spinnaker`
4. `sudo vim /opt/spinnaker/config/spinnaker-local.yml` 
5. `sudo vim /opt/rosco/config/rosco.yml`
6. sudo start spinnaker
7. Open http://localhost:9000
8. Create the Application, Pipeline, etc. similar to: http://www.spinnaker.io/docs/hello-spinnaker