resource "aws_db_subnet_group" "jamwiki" {
    name = "jamwiki"
    description = "jamwiki subnet group"
    subnet_ids = ["${aws_subnet.default.id}", "${aws_subnet.alt.id}"]
    tags {
        Name = "jamwiki"
    }
}

resource "aws_security_group" "jamwikidb" {
    vpc_id = "${aws_vpc.default.id}"
    name = "jamwikidb"
    description = "jamwiki database"

    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = ["${aws_vpc.default.cidr_block}"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_db_instance" "jamwiki" {
    name = "jamwiki"
    identifier = "jamwiki"
    allocated_storage = 5
    engine = "mysql"
    engine_version = "5.6.27"
    instance_class = "db.t1.micro"
    username = "${var.db_username}"
    password = "${var.db_password}"
    db_subnet_group_name = "${aws_db_subnet_group.jamwiki.id}"
    parameter_group_name = "default.mysql5.6"
    publicly_accessible = false
    port = 3306
    multi_az = true
    storage_type = "gp2"
    vpc_security_group_ids = ["${aws_security_group.jamwikidb.id}"]
}
